############################################################################################
##
## From Learning PowerShell DSC (Packt)
## by James Pogran (https://www.packtpub.com/networking-and-servers/learning-powershell-dsc)
##
############################################################################################
[CmdletBinding()]
param(
  $configData,
  $outputPath = ([IO.Path]::Combine($PSScriptRoot, 'HTTPSPullServer'))
)

Configuration HTTPSPullServer
{
  Import-DSCResource -ModuleName xWebAdministration
  Import-DSCResource -ModuleName xPSDesiredStateConfiguration
  Import-DSCResource -ModuleName PSDesiredStateConfiguration

  Node $AllNodes.Where({ $_.Roles -contains 'PullServer'}).NodeName
  {
    WindowsFeature DSCServiceFeature
    {
      Ensure = 'Present'
      Name   = 'DSC-Service'
    }

    File RegistrationKeyFile
    {
      Ensure          = 'Present'
      DestinationPath = (Join-Path $Node.RegistrationKeyPath $Node.RegistrationKeyFile)
      Contents        = $Node.RegistrationKey
      DependsOn       = @("[WindowsFeature]DSCServiceFeature")
    }

    xDscWebService PSDSCPullServer
    {
      Ensure                       = "Present"
      EndpointName                 = $Node.PullServerEndpointName
      Port                         = $Node.PullServerPort
      PhysicalPath                 = $Node.PullServerPhysicalPath
      CertificateThumbPrint        = $Node.CertificateThumbPrint
      ModulePath                   = $Node.ModulePath
      ConfigurationPath            = $Node.ConfigurationPath
      RegistrationKeyPath          = $Node.RegistrationKeyPath
      AcceptSelfSignedCertificates = $true
      UseSecurityBestPractices     = $true
      State                        = "Started"
      SqlProvider                  = $true
      SqlConnectionString          = 'Provider=SQLOLEDB.1;Server=.\SQLEXPRESS;Database=DSC;Trusted_Connection=Yes;Initial Catalog=master'
      #'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=master;Data Source=server2019;Database=DSC'
      DependsOn                    = @("[WindowsFeature]DSCServiceFeature","[File]RegistrationKeyFile")
    }
    
    Registry EnableTLS {
      Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server'
      ValueName = 'Enabled'
      ValueType = 'DWORD'
      ValueData = '1'
    }
  }
}

HTTPSPullServer -ConfigurationData $configData -OutputPath $outputPath
