$outputPath = ([IO.Path]::Combine($PSScriptRoot, 'FirefoxPolicies'))

Import-Module -Name "$($env:ProgramFiles)\WindowsPowerShell\Modules\xPSDesiredStateConfiguration\9.1.0\Modules\DscPullServerSetup\DscPullServerSetup.psm1"

.\basic_configuration.ps1 -ConfigData .\config_data_file.psd1
$moduleList = @('DSCR_Firefox','DSCR_Application','DSCR_FileContent')
Publish-DSCModuleAndMOF -Source $outputPath -ModuleNameList $moduleList

ls "$env:PROGRAMFILES\WindowsPowerShell\DscService\Configuration"