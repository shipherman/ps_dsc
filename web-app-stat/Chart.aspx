﻿<%@ Page Title="Chart" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       <div style=text-align: center; margin-top: 0px;">
            <div style="height: 40px; text-align: left">
                <a class="btn btn-default" href="/">Home</a>
            </div>
            <div style="text-align: center">
            <asp:Chart ID="Chart1" runat="server" DataSourceID="ChartSqlDataSource" ViewStateContent="All" BackColor="Azure" BorderlineColor="Transparent" IsMapEnabled="False" Palette="Excel" BorderlineWidth="0" Height="457px" Width="446px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="Status" YValueMembers="c" Label="#VAL" LegendText="#VALX">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX LineDashStyle="Dash">
                            <MajorGrid Enabled="False" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Titles>
                    <asp:Title Name="Title1">
                    </asp:Title>
                </Titles>
                <BorderSkin BackColor="Maroon" BackGradientStyle="LeftRight" SkinStyle="Raised" BorderColor="Transparent" />
            </asp:Chart>
            </div>
            <asp:SqlDataSource ID="ChartSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DSCConnectionString %>" SelectCommand="Select stat.Status, Count(stat.Status) c From (
Select distinct rd.NodeName, ISNULL(sr.Status, 'n/a') status FROM [DSC].[dbo].RegistrationData rd 
	LEFT OUTER JOIN (SELECT TOP (10000) * from [DSC].[dbo].StatusReport 
		WHERE StartTime &gt;= format (getdate(),'dd-MM-yy')) sr on sr.id = rd.AgentId 
		AND sr.EndTime = (SELECT MAX(EndTime) From [DSC].[dbo].StatusReport WHERE Id = sr.Id) 
) stat 
Group by Status"></asp:SqlDataSource>
        </div>
</asp:Content>
