﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>DSC Pull Server Statistics</h1>
        <p class="lead">Some simple tables and charts.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Charts</h2>
            <p>
                Charts here
            </p>
            <p>
                <a class="btn btn-default" href="/Chart">view &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Main Table</h2>
            <p>
                Status Table + status data
            </p>
            <p>
                <a class="btn btn-default" href="/Dashboard">view &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Some more data</h2>
            <p>
                I'll put some important info here. later
            </p>
            <p>
                <a class="btn btn-default" href="/Additional_Info">view &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
