﻿<%@ Page Title="Dash Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-sm-1" style="left: 14px; top: 13px">
             <a class="btn btn-default" href="/">Home</a>
        </div>
        <div class="col-sm-7">
           
               <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="614px">
                   <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                   <Columns>
                       <asp:BoundField DataField="NodeName" HeaderText="NodeName" SortExpression="NodeName" />
                       <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                       <asp:BoundField DataField="EndTime" HeaderText="EndTime" SortExpression="EndTime" />
                       <asp:BoundField DataField="InDesiredState" HeaderText="InDesiredState" SortExpression="InDesiredState" />
                       <asp:TemplateField HeaderText="Подробно">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="ShowConfig" Text="подробно" OnClick="LinkButton1_Click"></asp:LinkButton>
                        </ItemTemplate>
                       </asp:TemplateField>
                   </Columns>
                   <EditRowStyle BackColor="#999999" />
                   <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                   <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                   <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                   <RowStyle BackColor="#F7F6F3" ForeColor="#333333" Height="30px" HorizontalAlign="Left" />
                   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                   <SortedAscendingCellStyle BackColor="#E9E7E2" />
                   <SortedAscendingHeaderStyle BackColor="#506C8C" />
                   <SortedDescendingCellStyle BackColor="#FFFDF8" />
                   <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
               </asp:GridView>
               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSCConnectionString %>" SelectCommand="SELECT [NodeName], [Status], [EndTime], [InDesiredState] FROM [StatusView]"></asp:SqlDataSource>
        </div>
        <div class="col-sm-4">
            <asp:Label ID="Label1" runat="server" Text="Config Info"></asp:Label>
        </div>
    </div>
</asp:Content>


