[DscLocalConfigurationManager()]
Configuration HTTPSTargetNodeLCM
{
  Node $AllNodes.Where({ $_.Roles -contains 'all'}).NodeName
  {
    Settings
    {
      RefreshMode          = $Node.RefreshMode
      ConfigurationMode    = $Node.ConfigurationMode
      CertificateID        = $Node.CertificateID
      RebootNodeIfNeeded   = $Node.RebootNodeIfNeeded
      AllowModuleOverwrite = $Node.AllowModuleOverwrite
    }

    ConfigurationRepositoryWeb ConfigurationManager
    {
      ServerURL          = $Node.ConfigurationServerURL
      RegistrationKey    = $Node.RegistrationKey
      CertificateID      = $Node.CertificateID
      ConfigurationNames = $Node.NodeName
    }

    ReportServerWeb ReportManager
    {
      ServerURL = $Node.ConfigurationServerURL
    }
  }
}

$outputPath = '.\HTTPSTargetNodeLCM'
HTTPSTargetNodeLCM -ConfigurationData '.\config_data_file.psd1' -OutputPath $outputPath
Set-DscLocalConfigurationManager -Path $outputPath -Verbose