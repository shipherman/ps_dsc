$computers = get-content .\adwslist.csv
$proxy = get-content "\\sw03501608014\gp\PSFirefoxPolicies\Proxy.csv"
$config_data_file = '.\config_data_file.psd1'
$cert = Get-Childitem -Path Cert:\LocalMachine\My | where Subject -Like 'CN=<server>'

function WriteTo-File
{
    param
    (
    [string]$ComputerName,
    [string]$Roles = '@(all)'
    )
    $line = "    @{`n      NodeName = `"$ComputerName`"`n      Roles = $Roles`n    },"
    Add-content -Path $config_data_file -Value $line
}    

@"
@{
  AllNodes = @(
    @{
      NodeName               = "*"
      RegistrationKey        = '<reg key>'
      ConfigurationServerURL = "https://<server>:8080/psdscpullserver.svc/"
      CertificateID          = "$($cert.ThumbPrint)"
      RefreshMode            = "PULL"
      ConfigurationMode      = "ApplyAndMonitor"
      AllowModuleOverwrite   = `$true
      RebootNodeIfNeeded     = `$false
    },
"@ | Out-File $config_data_file

for ($comp in $computers)
{
    if ($comp -match "ws03501608004")
    {
        $roles = "@('all','avt','x86','firefox')"
        WriteTo-File -Roles $roles -ComputerName $comp
    }
    elseif ($comp -match "ws035016111..")
    {
        $roles = "@('all','ovp','x86')"
        WriteTo-File -Roles $roles -ComputerName $comp
    }
    elseif ($comp -match "ws035016110..")
    {
        $roles = "@('all','onp','x86')"
        WriteTo-File -Roles $roles -ComputerName $comp
    }
    elseif ($comp -match "ws035016100..")
    {
        $roles = "@('all','opu','x86')"
        WriteTo-File -Roles $roles -ComputerName $comp
    }
    elseif ($comp -match "ws035016211..")
    {
        if ($comp -in $proxy){
          $roles = "@('all','ksm','proxy')"
          WriteTo-File -Roles $roles -ComputerName $comp
        }
        else{
          $roles = "@('all','ksm')"
          WriteTo-File -Roles $roles -ComputerName $comp
        }
    }
}

"  );`n}" | Add-content -Path $config_data_file