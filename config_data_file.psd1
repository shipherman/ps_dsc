@{
  AllNodes = @(
    @{
      NodeName               = "*"
      RegistrationKey        = '<reg key>'
      ConfigurationServerURL = "https://<server>:8080/psdscpullserver.svc/"
      CertificateID          = "<sert>"
      RefreshMode            = "PULL"
      ConfigurationMode      = "ApplyAndMonitor"
      AllowModuleOverwrite   = $true
      RebootNodeIfNeeded     = $false
    },
    @{
      NodeName = "WS03501608004"
      Roles = @('all','avt','x86','firefox')
    },
    @{
      NodeName = "WS03501610001"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610002"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610003"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610004"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610005"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610006"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610007"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610008"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610009"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610010"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610011"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610012"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610013"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610014"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610015"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610016"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610017"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610018"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610019"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610020"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610021"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610022"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501610023"
      Roles = @('all','opu','x86')
    },
    @{
      NodeName = "WS03501611001"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611002"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611003"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611004"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611005"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611006"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611007"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611008"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611009"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611010"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611011"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611012"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611013"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611014"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611015"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611016"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611017"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611018"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611019"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611020"
      Roles = @('all','onp','x86')
    },
    @{
      NodeName = "WS03501611101"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611102"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611103"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611104"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611105"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611106"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611107"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611108"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611109"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611110"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611111"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611112"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611113"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611114"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611115"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611116"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611117"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611118"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611119"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501611120"
      Roles = @('all','ovp','x86')
    },
    @{
      NodeName = "WS03501621101"
      Roles = @('all','ksm')
    },
    @{
      NodeName = "WS03501621102"
      Roles = @('all','ksm')
    },
    @{
      NodeName = "WS03501621103"
      Roles = @('all','ksm','proxy')
    },
    @{
      NodeName = "WS03501621104"
      Roles = @('all','ksm','proxy')
    },
    @{
      NodeName = "WS03501621105"
      Roles = @('all','ksm')
    },
    @{
      NodeName = "WS03501621106"
      Roles = @('all','ksm','proxy')
    },
    @{
      NodeName = "WS03501621107"
      Roles = @('all','ksm')
    }
  );
}
