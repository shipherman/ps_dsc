$cert = Get-Childitem -Path Cert:\LocalMachine\My | where Subject -Like 'CN=sw03501608022.0035.pfr.ru'

$configData = @{
  AllNodes = @(
    @{
      NodeName               = "*"
      RegistrationKey        = '<reg key>'
      ConfigurationServerURL = "https://<server>:8080/psdscpullserver.svc/"
      CertificateID          = "$($cert.ThumbPrint)"
      RefreshMode            = "PULL"
      ConfigurationMode      = "ApplyAndAutocorrect"
      AllowModuleOverwrite   = $true
      RebootNodeIfNeeded     = $false
    },
    @{
      NodeName                     = 'sw03501608022'
      Roles                        = @('PullServer')
      AcceptSelfSignedCertificates = $true
      CertificateThumbPrint        = "$($cert.ThumbPrint)"
      RegistrationKeyFile          = 'RegistrationKeys.txt'
      RegistrationKeyPath          = "$env:PROGRAMFILES\WindowsPowerShell\DscService"
      ModulePath                   = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Modules"
      ConfigurationPath            = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Configuration"
      PullServerEndpointName       = 'PSDSCPullServer'
      PullServerPhysicalPath       = "$env:SystemDrive\inetpub\PSDSCPullServer"
      PullServerPort               = 8080
    }
  );
}

return $configData
