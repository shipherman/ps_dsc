[CmdletBinding()]
param(
  $ConfigData,
  $OutputPath = ([IO.Path]::Combine($PSScriptRoot, 'FirefoxPolicies'))
)

Configuration FirefoxPolicies 
{
  Import-DscResource -Module PSDscResources
  Import-DscResource -Module DSCR_Firefox
  Import-DscResource -Module DSCR_Application
  Import-DscResource -Module DSCR_Filecontent
  
  Node $AllNodes.Where({($_.Roles -contains 'firefox') -and ($_.Roles -contains 'x86')}).NodeName
  {
    cFirefox InstallFirefox
    {
      VersionNumber = '75.0'
      Language = 'ru'
      MachineBits = 'x86'
      InstallerPath = '\\sw03501608014\gp\PSInstall\Firefox\Firefox75.0(86).msi'
    }
  }
  
  Node $AllNodes.Where({($_.Roles -contains 'all') -and ($_.Roles -contains 'x86')}).NodeName
  {
   File LibreOfficeTemplateODT
    {
      Ensure = 'Present'
      Type = 'File'
      SourcePath = '\\sw03501608014\gp\PSLibreOffice\Template\soffice.odt'
      DestinationPath = 'C:\Program Files\LibreOffice\share\template\shellnew\soffice.odt'
      Checksum = 'SHA-256'
    }
  }
  
  Node $AllNodes.Where({$_.Roles -contains 'all'}).NodeName
  {
   File Preferences
    {
      Ensure = 'Present'
      Type = 'File'
      SourcePath = '\\sw03501608014\gp\PSFirefoxPolicies\all\autoconfig.js'
      DestinationPath = 'c:\Program Files\Mozilla Firefox\defaults\pref\autoconfig.js'
      Checksum = 'SHA-256'
    }
  }
  
  Node $AllNodes.Where({($_.Roles -contains 'all') -and -not ($_.Roles -contains 'proxy')}).NodeName
  {
    File PoliciesDir
    {
      Ensure = 'Present'
      Type = 'Directory'
      DestinationPath = 'C:\Program Files\Mozilla Firefox\distribution'
    }
    
    File Policies
    {
      Ensure = 'Present'
      Type = 'File'
      SourcePath = '\\sw03501608014\gp\PSFirefoxPolicies\all\distribution\policies.json'
      DestinationPath = 'C:\Program Files\Mozilla Firefox\distribution\policies.json'
      Checksum = 'SHA-256'
      DependsOn = '[File]PoliciesDir'
    }
  }
    
  Node $AllNodes.Where({($_.Roles -contains 'ksm') -and ($_.Roles -contains 'proxy')}).NodeName
  {
    File PoliciesDir
    {
      Ensure = 'Present'
      Type = 'Directory'
      DestinationPath = 'C:\Program Files\Mozilla Firefox\distribution'
    }
    
    File Policies
    {
      Ensure = 'Present'
      Type = 'File'
      SourcePath = '\\sw03501608014\gp\PSFirefoxPolicies\mih_proxy\distribution\policies.json'
      DestinationPath = 'C:\Program Files\Mozilla Firefox\distribution\policies.json'
      Checksum = 'SHA-256'
      DependsOn = '[File]PoliciesDir'
    }
  }
  
  Node $AllNodes.Where({$_.Roles -contains 'test'}).NodeName
  {
    File testdir
    {
      Ensure = 'Present'
      Type = 'Directory'
      DestinationPath = 'C:\TEST_DIR'
    }
	
    File TotalCommander
    {
      Type = 'Directory'
      SourcePath = '\\sw03501608014\gp\PSInstall\TotalCommander\totalcmd'
      DestinationPath = 'C:\totalcmd'
      Recurse = $true
    }
  }
}



FirefoxPolicies -OutputPath $OutputPath -ConfigurationData $ConfigData
      